package main

import (
	"log"
	"os"
	"net/http"
	"github.com/gorilla/mux"
	"encoding/json"
	"strconv"
)

type Student struct {
	ID        string `json:"id,omitempty"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Group     string `json:"group"`
}

func GetStudents(data map[int]Student, w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(data)
}

func GetStudent(data map[int]Student, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	if id, err := strconv.ParseInt(params["id"], 0, 0); err == nil {
		if item, ok := data[int(id)]; ok {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(Student{})
}

func AddStudent(data map[int]Student, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var person Student
	_ = json.NewDecoder(r.Body).Decode(&person)
	person.ID = params["id"]
	log.Println(person)

	if id, err := strconv.ParseInt(params["id"], 0, 0); err == nil {
		id := int(id)
		if _, ok := data[id]; !ok {
			data[id] = person
		}
	}
	json.NewEncoder(w).Encode(data)
}

func DeleteStudent(data map[int]Student, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	if id, err := strconv.ParseInt(params["id"], 0, 0); err == nil {
		delete(data, int(id))
	}
	json.NewEncoder(w).Encode(data)
}

func init() {
	log.SetOutput(os.Stdout)
}

func main() {
	data := map[int]Student{
		1: {ID: "1", FirstName: "Ivan", LastName: "Ivanov", Group: "IVT"},
		2: {ID: "2", FirstName: "Vasya", LastName: "Gavrilov", Group: "RT"},
		3: {ID: "3", FirstName: "Kirill", LastName: "Balabanov", Group: "SMT"},
	}

	router := mux.NewRouter()

	router.HandleFunc("/students", func(writer http.ResponseWriter, request *http.Request) {
		GetStudents(data, writer, request)
	}).Methods("GET")

	router.HandleFunc("/students/{id}", func(writer http.ResponseWriter, request *http.Request) {
		GetStudent(data, writer, request)
	}).Methods("GET")

	router.HandleFunc("/students/{id}", func(writer http.ResponseWriter, request *http.Request) {
		AddStudent(data, writer, request)
	}).Methods("POST")

	router.HandleFunc("/students/{id}", func(writer http.ResponseWriter, request *http.Request) {
		DeleteStudent(data, writer, request)
	}).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8080", router))
}

//type Page struct {
//	Name string
//	Data []byte
//}
//
//func (p Page) show() {
//	log.Println(p)
//}
//
//func (p Page) String() string {
//	return fmt.Sprintf("\nTitle: \"%s\"\nBody {\n\t%s\n}", p.Name, p.Data)
//}
//
//func loadPage(title string) (*Page, error) {
//	body, err := ioutil.ReadFile(title + ".html")
//	if err != nil {
//		return nil, err
//	}
//	return &Page{Name: title, Data: body}, nil
//}
//
//
//func viewHandler(route string, responseWriter http.ResponseWriter, request *http.Request) {
//	title := request.URL.Path[len(route):]
//	if p, err := loadPage(title); err != nil {
//		fmt.Fprintf(responseWriter, "<p>page \"%s\" not found</p>", title)
//		//log.Printf("page \"%s\" not found", title)
//	} else {
//		fmt.Fprintf(responseWriter, "<h1>%s</h1><div>%s</div>", p.Name, p.Data)
//		log.Printf(string(p.Data))
//	}
//}
